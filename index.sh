#!/usr/bin/env bash

BASH_UTILS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source $BASH_UTILS/terminal_format.sh
source $BASH_UTILS/functions.sh
