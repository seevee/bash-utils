#!/usr/bin/env bash
# basic functions

# clean overwrite
# $1: symlink
# $2: destination
overwrite_symlink () {
  unlink "$2" || rm -r "$2"
  ln -ns "$1" "$2"
}

# $1: file
# $2: add string
# $3: check string
append_if_missing () {
  grep -q "$2" "$1" || echo "$3" >> "$1"
}

# $1: add string
# $2: check string (optional, defaults to add string)
append_rc () {
  append_if_missing "$HOME/.bashrc" "$1" "${2:$1}"
  append_if_missing "$HOME/.zshrc" "$1" "${2:$1}"
}

# based on terdon on SO: https://askubuntu.com/a/459425/548109
get_linux_distribution () {
  local uname
  uname=$(uname | tr "[:upper:]" "[:lower:]")
  if [ "$uname" != "linux" ]; then
    echo "$uname"
    return
  fi
  # if available, use LSB to identify distribution
  if [ -f /etc/lsb-release ] || [ -d /etc/lsb-release.d ]; then
    lsb_release -i | cut -d: -f2 | sed s/'^\t'// | tr "[:upper:]" "[:lower:]"
    return
  fi
  # otherwise, use release info file
  for file in /etc/[A-Za-z]*[_-][rv]e[lr]*; do
    if [[ "$file" == *lsb* ]]; then
      continue
    fi
    echo "$file" | \
      cut -d'/' -f3 | cut -d'-' -f1 | cut -d'_' -f1 | \
      tr "[:upper:]" "[:lower:]"
  done
}

# requires sudo
# $1: array of dependencies
install_packages () {
  local IFS=" "
  case "$OSTYPE" in
    linux*)
      local distro
      distro=$(get_linux_distribution)
      case "$distro" in
        *ubuntu* | *pop*)       sudo apt-get install "${@}" -y;;
        *endeavouros* | *arch*) sudo pacman -Syu "${@}" --noconfirm --needed;;
        *fedora*)               sudo dnf install "${@}" -y;;
        *)                      echo "unsupported distro: $distro"; exit 1;;
      esac;;
    darwin*)                    brew install "${@}";;
    win* | msys* | cygwin* | \
    bsd* | solaris* | *)        echo "unsupported OS: $OSTYPE"; exit 1;;
  esac
}
